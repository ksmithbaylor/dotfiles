# Path
OS="`uname`"
case $OS in
    'Darwin')
        PATH=$HOME/.cabal/bin
        PATH=$PATH:$HOME/.nodebrew/current/bin
        PATH=$PATH:/Developer/NVIDIA/CUDA-6.0/bin
        PATH=$PATH:/Applications/Postgres.app/Contents/Versions/9.3/bin
        PATH=$PATH:/usr/local/bin                          # homebrew binaries
        PATH=$PATH:/usr/local/sbin
        PATH=$PATH:/usr/local/share/npm/bin                # npm-installed binaries
        PATH=$PATH:/usr/bin:/bin:/usr/sbin:/sbin           # default system path
        PATH=$PATH:~/bin
        PATH=$PATH:/usr/texbin
        PATH=$PATH:/usr/local/mysql/bin
    ;;
    'Linux')
        PATH=~/local/java/bin:$PATH
        PATH=~/local/bin:$PATH
        PRINTER="ecs214"
    ;;
esac

PROMPT_COMMAND='PS1="\[\033[0;33m\][\T]\`if [[ \$? = "0" ]]; then echo "\\[\\033[32m\\]"; else echo "\\[\\033[31m\\]"; fi\` \`if [[ `pwd|wc -c|tr -d " "` > 18 ]]; then echo "\\W"; else echo "\\w"; fi\` \$\[\033[0m\] "; echo -ne "\033]0;`hostname -s`:`pwd`\007"'

# Enable colors for ls
export CLICOLOR=1

# Default text editor is vim for commit messages, etc
export EDITOR=vim

# Keep up to 10000 commands in history
export HISTFILESIZE=10000

if [ "$OS" == "Darwin" ]; then
    # Fix newest git version on mac
    git config --global push.default simple
    # rbenv settings (only executes if rbenv is installed)
    if which rbenv > /dev/null; then
        export RBENV_ROOT=/usr/local/var/rbenv # Use homebrew directory instead of ~/.rbenv
        eval "$(rbenv init -)"
    fi
fi
