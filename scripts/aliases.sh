OS="`uname`"

# OS-specific
alias reload="source ~/.bashrc"
case $OS in
    'Darwin')
        alias l="ls -1"
        alias ll="ls -Ap1"
        alias lll="ls -oAhG"
        alias edit="vim ~/dotfiles"
    ;;
    'Linux')
        alias l="ls --color -1"
        alias ll="ls --color -Ap1"
        alias lll="ls --color -oAhG"
        alias edit="vim ~/dotfiles"
    ;;
esac

# Navigation
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"
alias c="clear"
alias x="exit"

# Listing files
alias t="tree -ACa --dirsfirst"
alias pathcount="echo -ne 'Total commands in \$PATH: ' && echo $PATH | sed -e s/:/\ /g | xargs ls -1 | grep -v ':' | sed '/^$/d' | wc -l | sed 's/ //g'"

# Program shortcuts
alias o="open"
alias s="subl ."
alias tu="top -o cpu"
alias tm="top -o vsize"
alias ed="ed -p '--> '"
alias myip='dig +short myip.opendns.com @resolver1.opendns.com'
alias clip="nc localhost 8377"
alias brake="bundle exec rake"

function ove() {
    if [[ $1 == *-* ]]; then
        sshpass -p "thought" ssh tworker@$1.ove.local
    else
        sshpass -p "thought" ssh tworker@$1.onyx.ove.com
    fi
}

# Git shortcuts
alias g="git"
alias st="git status"
alias stp="repeat 1 'clear && git status'"
alias cm="git commit -m"
alias cma="git commit -am"
alias br="git branch -v"
alias co="git checkout"
alias a="git add"
alias gd="git diff"
alias lg="git lg"
alias gr="git lg --all"
alias push="git push"
alias pull="git pull"

# vim pager
VLESS=$(find /usr/share/vim -name 'less.sh')
if [ ! -z $VLESS ]; then
  alias less=$VLESS
fi

# Vim printer
# Will print via lpr to $PRINTER if $PRINTER is defined
# Otherwise, it will just save pdfs to print later
function printall {
    for file in $@; do
        vim "+colorscheme newsprint" -c "hardcopy > $file.ps" -c "quit" $file
        ps2pdf $file.ps
        rm $file.ps
    done

    if [ ! -z $PRINTER ]; then
        for file in $@; do
            lpr -P $PRINTER $file.pdf
            sleep 1
            rm $file.pdf
        done
    fi
}

# Programming and compiling shortcuts
function trimextension {
    BASE=$(basename "$1")
    echo "${BASE%.*}"
}

function runjava {
    RJFILE=$1
    RJCLASS=$(trimextension $1)
    shift
    echo "--- Compiling $RJFILE..."
    javac $RJFILE
    if [ $? -eq 0 ]; then
        echo -ne "--- Running the $RJCLASS class"
        if [ "$#" -ne 0 ]; then
            echo " with arguments '$@'"
            java $RJCLASS $@
        else
            echo
            java $RJCLASS
        fi
        rm *.class 2> /dev/null
    else
        echo; echo "There were compilation issues."
    fi
}

function csi3336gcc {
    gcc -ansi -pedantic -Wall -o `trimextension $1` $1
}

function bin {
    if [[ $1 == "-r" ]]; then
        shift
        str=""
        for s in $@; do str=$str$s; done
        ruby -e "puts '$str'.to_i(2).to_s"
    else
        ruby -e "puts $1.to_s(2).reverse.scan(/.{1,4}/).map{|s|s.ljust(4,'0')}.join(' ').reverse"
    fi
}

function hex {
    if [[ $1 == "-r" ]]; then
        shift
        str=""
        for s in $@; do str=$str$s; done
        ruby -e "puts '$str'.to_i(16).to_s"
    else
        ruby -e "puts $1.to_s(16).reverse.scan(/../).map{|s|s.ljust(2,'0')}.join(' ').reverse"
    fi
}

function hexr {
    ruby -e "puts $1.gsub(/ /,'').to_i(16).to_s"
}


# SSH shortcuts
alias baylor="ssh smithke@earth.ecs.baylor.edu"

# Useful functions
function mdcd { # creates a directory and then cds into it
    mkdir $1
    cd $1
}

if [ "$OS" == "Darwin" ]; then
    function hl { # Prints out a file with syntax highlighting
        echo "" # For visual spacing
        # Highlight with 'moria' theme to terminal, and suppress errors
        highlight $1 -s moria -O xterm256 2> /dev/null

        if (($? != 0)); then # If the command had errors
            cat $1 # Just cat the file out instead
        fi

        echo "" # For visual spacing
    }

    function hlprint {
        # Print with line numbers and 'moria' theme
        highlight $1 -l -o print.html -s moria
        open print.html # Open in browser
        sleep 5 # Give the browser time to open
        rm print.html highlight.css # Remove output files
    }
fi

function gh {
    git clone https://github.com/$1/$2.git
}

function colors {
  #   Daniel Crisman's ANSI color chart script from
  #   The Bash Prompt HOWTO: 6.1. Colours
  #   http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html
  #
  #   This function echoes a bunch of color codes to the
  #   terminal to demonstrate what's available.  Each
  #   line is the color code of one forground color,
  #   out of 17 (default + 16 escapes), followed by a
  #   test use of that color on all nine background
  #   colors (default + 8 escapes).
  #

  T='gYw'   # The test text

  echo -e "\n         def     40m     41m     42m     43m     44m     45m     46m     47m";

  for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' \
             '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' \
             '  36m' '1;36m' '  37m' '1;37m';

    do FG=${FGs// /}
    echo -en " $FGs \033[$FG  $T  "

    for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
      do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
    done
    echo;
  done
  echo
}

function repeat {
    NUM="$1"
    while [ true ]; do
        eval $2;
        sleep $NUM
    done
}
