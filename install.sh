#!/bin/bash

DOTFILESDIR=$(cd $(dirname $0); pwd -P)
DIRNAME="dots"

if [[ -e ~/.vim ]]; then rm ~/.vim; fi
for dotfile in $(ls -A $DOTFILESDIR/$DIRNAME); do
    fullpath=$DOTFILESDIR/$DIRNAME/$dotfile
    ln -fs $fullpath ~/$dotfile
done

if [[ -e ~/.scripts ]]; then rm ~/.scripts; fi
ln -fs $DOTFILESDIR/scripts ~/.scripts

source ~/.bashrc
